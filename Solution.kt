class Solution {
    fun reverse(x: Int): Int {
        return when {
            (x == Integer.MIN_VALUE) -> 0
            else -> {
                var num = x
                var isNegative : Boolean = false
                var answer : Int = 0
                if(x < 0) {
                    isNegative = true
                    num = num*(-1)
                }
                while(num != 0) {
                    val mod = num % 10
                    if (answer > (Integer.MAX_VALUE - mod) / 10) return 0;
                    answer = answer*10 + mod
                    num = num / 10
                }
                if(isNegative) answer * (-1) else answer
            }
        }
    }
}